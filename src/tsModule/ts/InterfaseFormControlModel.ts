import { EventEmitter } from './eventEmitter';
export interface FormControlModel<V = any> {
  status: V;
  value: V;
  id: string;
  valueChange: EventEmitter<{  inputIdValue: V, statusValue: V }>;
  statusChange: EventEmitter<{ inputId: string, value: boolean }>;
}