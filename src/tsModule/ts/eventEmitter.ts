type EventEmitterHandler<M> = (arg: M) => void;
//«транслятор» или «эмиттер» событий
export class EventEmitter<T> {
  private handlers: Array<EventEmitterHandler<T>>
  constructor() {
    this.handlers= [];
  }
  // emit генерирует события, которые мы хотим транслировать
  emit(eventData: T) {
    if (this.handlers) {
      this.handlers.forEach(eventSubscr => eventSubscr(eventData));
    }
  }
  //subscribe принимает  события и обрабатывает
  subscribe(eventSubscr: EventEmitterHandler<T>) {
    if (!this.handlers) {
      this.handlers = [];
    }

    this.handlers.push(eventSubscr);
  }

}
// type EventEmitterHandler<M> = (arg: M) => void;

// export class EventEmitter<T> {
    
//     handlers: Array<EventEmitterHandler<T>>

//     constructor() {
//         this.handlers = [];
//     }

//     emit(data: T) {
//         this.handlers.forEach(handler => handler(data));
//     }

//     subscribe(event: EventEmitterHandler<T>) {
//         this.handlers.push(event);
//     }
// }
