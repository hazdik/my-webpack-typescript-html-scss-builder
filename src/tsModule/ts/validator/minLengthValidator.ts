import { Validator } from './validator';

//валидация на минимальное количество символов
export class MinLengthValidator extends Validator {
  constructor(private minLength: number) {
    super();

    this.minLength = minLength;

  }
  validate(value: string): boolean {
    return value.length > this.minLength
  }
}
