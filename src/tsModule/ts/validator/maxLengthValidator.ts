import { Validator } from './validator';

// валидация на мфксимальное количество символов
export class MaxLengthValidator extends Validator {
  constructor(private maxLength: number) {
    super();

    this.maxLength = maxLength;

  }
  validate(value: string): boolean {
    return value.length < this.maxLength
  }
}
