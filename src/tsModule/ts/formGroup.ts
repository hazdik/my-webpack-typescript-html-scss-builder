import { EventEmitter } from './eventEmitter';
import { FormControl } from './formControl';
import { FormControlModel } from './InterfaseFormControlModel';

export class FormGroup implements FormControlModel{

  public status: boolean | Array<boolean>;
  public value: object;
  public valueChange: EventEmitter<{ inputIdValue: object, statusValue: boolean }>;
  public statusChange: EventEmitter<{ inputId: string, value: boolean }>;

  constructor(public id: string, private controls: Array<FormControl>) {
    this.id = id;
    this.controls = controls;
    this.status = false;
    this.value = {};
    this.valueChange = new EventEmitter();
    this.statusChange = new EventEmitter();

    // перебираем массив контроллеров
    this.controls.forEach((element: FormControl) => {

      //принимаем и обрабатываем события
      element.valueChange.subscribe((changeElem: object) => {

        //получаем статус каждого контролера
        this.status = this.controls.map(function (elem: FormControl) {
          return elem.status;
        });
        // проверяем все статусы на тру
        this.status = this.status.every(function (valid: boolean): boolean {
          return valid === true;
        });

        this.value = this.controls.map(function (elem: FormControl) {
          return elem.value;
        });

        this.valueChange.emit({
          inputIdValue: this.value,
          statusValue: this.status

        });
      });
    });
  }
}
